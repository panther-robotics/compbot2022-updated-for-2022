package frc.robot.commands.autoPaths;

import edu.wpi.first.wpilibj2.command.Subsystem;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.AutoCollect;
import frc.robot.commands.AutoDrive;
import frc.robot.commands.AutoEject;
import frc.robot.commands.AutoTurnBasic;
import frc.robot.commands.climber.AutoClimb;
import frc.robot.commands.AutoDrive;
import frc.robot.commands.shooter.AutoShoot;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.Shooter.ShotSpeed;

public class MotionMagicPaths {

    private static final double HUB_LENGTH = 36.83;
    private static final double ROBOT_LENGTH = 29;
    private static final double COLLECTOR_LENGTH = 14;
    private static final double HUB_TO_VERTEX = 84.75; //center of hub to point on tarmac
    private static final double VERTEX_ANGLE_FROM_TARMAC_SIDE= 67.5; //if facing tower at tarmac middle point 
    private static final double DISTANCE_TO_ANGLE_HORIZONTAL = 100;

    public enum AutoPosition{POSITION_0, POSITION_1, POSITION_2}

      /**
   * drives robot 100 inches forward
   * @param drivetrain westcoast style drivetrain
   * @return command that runs the path
   */
    public static Command driveForward(Subsystem drivetrain){
        return new AutoDrive(drivetrain, 100);
    }
      /**
   * drive robot 60 inches backward
    * @param drivetrain westcoast style drivetrain
   * @return command that runs the path
   */
    private static Command driveBackward(Subsystem drivetrain){
        return new AutoDrive(drivetrain, -60);
    }
     /**
   * rotates climb horizontial, shoots ball, then drives backward 60 inches
   * @param drivetrain drivetrain system to be required
   * @param shooter shooter system to be required
   * @param climber climber system to be required
   * @return command that runs the path
   */
    public static Command shootAndCrossLine(Subsystem drivetrain, Subsystem shooter, Subsystem climber){
        Command rotate = new AutoClimb(climber);
        return new SequentialCommandGroup(rotate, new AutoShoot(shooter, ShotSpeed.LOW), driveBackward(drivetrain));
    }
    
    public static Command oldTwoBallPosition0(Subsystem climber, Subsystem drivetrain, Subsystem shooter, Subsystem collector){
        Command rotate = new AutoClimb(climber);
        Command shoot = new AutoShoot(shooter, ShotSpeed.MID);
        //158.95
        Command drive = new AutoDrive(drivetrain, -50); //drive backwards so robot center is on tarmaic vertex
        Command turn = new AutoTurnBasic(drivetrain, 85); // align robot to face cargo       
        Command driveToCargo = new AutoDrive(drivetrain, 45);
        ParallelRaceGroup driveAndCollect = new ParallelRaceGroup(driveToCargo, new AutoCollect(collector));
        Command driveFromCargo = new AutoDrive(drivetrain, -45);
        Command turnBack = new AutoTurnBasic(drivetrain, -85);
        Command driveToStartingPosition = new AutoDrive(drivetrain, -50); //drive backwards so robot center is on tarmaic vertex
        return new SequentialCommandGroup(rotate, shoot, drive, turn, driveAndCollect, driveFromCargo, turnBack, driveToStartingPosition);
    }
    /**
   * rotates climb horizontial, shoots ball, drives backwards, turns & drives to collect ball, turn & drives to collect ball2, turns & drives to hub, shoots balls
   * @param drivetrain drivetrain system to be required
   * @param shooter shooter system to be required
   * @param climber climber system to be required
   * @return command that runs the path
   */
    public static Command threeBallPosition0(Subsystem climber, Subsystem drivetrain, Subsystem shooter, Subsystem collector){
        Command rotate = new AutoClimb(climber);
        Command shoot = new AutoShoot(shooter, ShotSpeed.MID);
        Command drive = new AutoDrive(drivetrain, -45); //drive backwards so robot center is on tarmac vertex
        Command turnToBall1 = new AutoTurnBasic(drivetrain, 92); // align robot to cargo near wall      
        Command driveToCargo = new AutoDrive(drivetrain, 45); 
        ParallelRaceGroup driveAndCollect = new ParallelRaceGroup(driveToCargo, new AutoCollect(collector));
        Command turnToBall2 = new AutoTurnBasic(drivetrain, -40); //decrease to 35 if increased turn to 95
        Command driveToCargo2 = new AutoDrive(drivetrain, 100);
        ParallelRaceGroup driveAndCollect2 = new ParallelRaceGroup(driveToCargo2, new AutoCollect(collector));
        Command turnToHub = new AutoTurnBasic(drivetrain, -122);
        Command driveToHub = new AutoDrive(drivetrain, 45);
        Command shoot2 = new AutoShoot(shooter, ShotSpeed.MID);
        Command shoot3 = new AutoShoot(shooter, ShotSpeed.MID);
        return new SequentialCommandGroup(rotate, shoot, drive, turnToBall1, driveAndCollect, turnToBall2, driveAndCollect2, turnToHub, driveToHub, shoot2, shoot3);
    }
    public static Command diffThreeballPosition0(Subsystem climber, Subsystem drivetrain, Subsystem shooter, Subsystem collector){
        Command rotate = new AutoClimb(climber);
        Command shoot = new AutoShoot(shooter, ShotSpeed.LOW);
        Command drive = new AutoDrive(drivetrain, -22.5); //drive backwards so robot center is on tarmac vertex
        Command turnToBall1 = new AutoTurnBasic(drivetrain, 140); // align robot to cargo near tarmac line  //143     
        Command driveToCargo = new AutoDrive(drivetrain, 22.5); 
        ParallelRaceGroup driveAndCollect = new ParallelRaceGroup(driveToCargo, new AutoCollect(collector));
        Command turnToBall2 = new AutoTurnBasic(drivetrain, -81); //-82
        ParallelRaceGroup turnToBall2AndCollect = new ParallelRaceGroup(turnToBall2, new AutoCollect(collector));
        Command driveToCargo2 = new AutoDrive(drivetrain, 112); //107
        ParallelRaceGroup driveAndCollect2 = new ParallelRaceGroup(driveToCargo2, new AutoCollect(collector));
        Command turnToHub = new AutoTurnBasic(drivetrain, -125); //-127
        ParallelRaceGroup turnToHubAndCollect = new ParallelRaceGroup(turnToHub, new AutoCollect(collector));
        Command driveToHub = new AutoDrive(drivetrain, 35); //35
        ParallelRaceGroup shoot2 = new ParallelRaceGroup(new AutoShoot(shooter, ShotSpeed.HIGH, 4), new AutoEject(collector));
        return new SequentialCommandGroup(rotate, shoot, drive, turnToBall1, driveAndCollect, turnToBall2AndCollect, driveAndCollect2, turnToHubAndCollect, driveToHub, shoot2);
     }
    
    //  public static Command diffThreeballPosition0(Subsystem climber, Subsystem drivetrain, Subsystem shooter, Subsystem collector){
    //     Command rotate = new AutoClimb(climber);
    //     Command shoot = new AutoShoot(shooter, ShotSpeed.MID);
    //     Command drive = new AutoDrive(drivetrain, -22.5); //drive backwards so robot center is on tarmac vertex
    //     Command turnToBall1 = new AutoTurnBasic(drivetrain, 147); // align robot to cargo near tarmac line       
    //     Command driveToCargo = new AutoDrive(drivetrain, 22.5); 
    //     ParallelRaceGroup driveAndCollect = new ParallelRaceGroup(driveToCargo, new AutoCollect(collector));
    //     Command turnToBall2 = new AutoTurnBasic(drivetrain, -82); //decrease to 35 if increased turn to 95
    //     ParallelRaceGroup turnToBall2AndCollect = new ParallelRaceGroup(turnToBall2, new AutoCollect(collector));
    //     Command driveToCargo2 = new AutoDrive(drivetrain, 107); //100
    //     ParallelRaceGroup driveAndCollect2 = new ParallelRaceGroup(driveToCargo2, new AutoCollect(collector));
    //     Command turnToHub = new AutoTurnBasic(drivetrain, -120); //118
    //     ParallelRaceGroup turnAndCollect = new ParallelRaceGroup(turnToHub, new AutoCollect(collector));
    //     Command driveToHub = new AutoDrive(drivetrain, 35); //45
    //     ParallelRaceGroup shoot2 = new ParallelRaceGroup(new AutoShoot(shooter, ShotSpeed.MID, 5), new AutoEject(collector));
    //     ParallelRaceGroup shoot3 = new ParallelRaceGroup(new AutoShoot(shooter, ShotSpeed.MID), new AutoEject(collector));
    //     return new SequentialCommandGroup(rotate, shoot, drive, turnToBall1, driveAndCollect, turnToBall2AndCollect, driveAndCollect2, turnAndCollect, driveToHub, shoot2);
    //  }
     public static Command diffFiveballPosition0(Subsystem climber, Subsystem drivetrain, Subsystem shooter, Subsystem collector){
        Command rotate = new AutoClimb(climber);
        Command shoot = new AutoShoot(shooter, ShotSpeed.MID);
        Command drive = new AutoDrive(drivetrain, -22.5); //drive backwards so robot center is on tarmac vertex
        Command turnToBall1 = new AutoTurnBasic(drivetrain, 147); // align robot to cargo near tarmac line       
        Command driveToCargo = new AutoDrive(drivetrain, 22.5); 
        ParallelRaceGroup driveAndCollect = new ParallelRaceGroup(driveToCargo, new AutoCollect(collector));
        Command turnToBall2 = new AutoTurnBasic(drivetrain, -82); //decrease to 35 if increased turn to 95
        ParallelRaceGroup turnToBall2AndCollect = new ParallelRaceGroup(turnToBall2, new AutoCollect(collector));
        Command driveToCargo2 = new AutoDrive(drivetrain, 107); //100
        ParallelRaceGroup driveAndCollect2 = new ParallelRaceGroup(driveToCargo2, new AutoCollect(collector));
        Command turnToHub = new AutoTurnBasic(drivetrain, -120); //118
        Command driveToHub = new AutoDrive(drivetrain, 35); //45
        ParallelRaceGroup shoot2 = new ParallelRaceGroup(new AutoShoot(shooter, ShotSpeed.MID), new AutoEject(collector));
        ParallelRaceGroup shoot3 = new ParallelRaceGroup(new AutoShoot(shooter, ShotSpeed.MID), new AutoEject(collector));
        //untested 5 ball auto:
        Command driveBackwards = new AutoDrive(drivetrain, -50);
        Command turnToBall2And3 = new AutoTurnBasic(drivetrain, 170);
        Command driveToBall2And3 = new AutoDrive(drivetrain, 120);
        ParallelRaceGroup driveAndCollect3 = new ParallelRaceGroup(driveToBall2And3, new AutoCollect(collector));
        Command turnToHub2 = new AutoTurnBasic(drivetrain, -180);
        Command driveToHub2 = new AutoDrive(drivetrain, 165);
        Command shoot4 = new AutoShoot(shooter, ShotSpeed.MID);
        Command shoot5 = new AutoShoot(shooter, ShotSpeed.MID);
        return new SequentialCommandGroup(rotate, shoot, drive, turnToBall1, driveAndCollect, turnToBall2AndCollect, driveAndCollect2, turnToHub, driveToHub, shoot2, shoot3,
         driveBackwards, turnToBall2And3, driveAndCollect3, turnToHub2, driveToHub2, shoot4, shoot5);
    }
     public static Command twoBallPosition0(Subsystem climber, Subsystem drivetrain, Subsystem shooter, Subsystem collector){
        Command rotate = new AutoClimb(climber);
        Command shoot = new AutoShoot(shooter, ShotSpeed.MID);
        Command drive = new AutoDrive(drivetrain, -22.5); //drive backwards so robot center is on tarmac vertex
        Command turnToBall1 = new AutoTurnBasic(drivetrain, -147); // align robot to cargo near tarmac line       
        Command driveToCargo = new AutoDrive(drivetrain, 22.5); 
        Command turnToHub = new AutoTurnBasic(drivetrain, -160); // align robot to cargo near tarmac line       
        Command driveToHub = new AutoDrive(drivetrain, 35); 
        Command shoot2 = new AutoShoot(shooter, ShotSpeed.MID, 2);
        return new SequentialCommandGroup(rotate, drive, turnToBall1, driveToCargo, turnToHub, driveToHub, shoot);
    }
    //angles from "practice field" in the commons, might not be correct on real field 
    public static Command twoBallPosition1(Subsystem climber, Subsystem drivetrain, Subsystem shooter, Subsystem collector){
        Command rotate = new AutoClimb(climber);
        Command shoot = new AutoShoot(shooter, ShotSpeed.LOW);
        Command drive = new AutoDrive(drivetrain, -22.5); //drive backwards so robot center is on tarmac vertex
        Command turnToBall1 = new AutoTurnBasic(drivetrain, -137); // align robot to cargo near tarmac line  132 original   
        Command driveToCargo = new AutoDrive(drivetrain, 40); //25 original 
        ParallelRaceGroup driveAndCollect = new ParallelRaceGroup(driveToCargo, new AutoCollect(collector));
        Command turnToHub = new AutoTurnBasic(drivetrain, 156); // align robot to cargo near tarmac line       //140 original
        ParallelRaceGroup turnAndCollect = new ParallelRaceGroup(turnToHub, new AutoCollect(collector));
        Command driveToHub = new AutoDrive(drivetrain, 47.5);  //30 original
        Command shoot2 = new AutoShoot(shooter, ShotSpeed.LOW);
        return new SequentialCommandGroup(rotate, shoot, drive, turnToBall1, driveAndCollect, turnAndCollect, driveToHub, shoot2);
    }
}
//     public static Command shootAndCollectPosition1(Subsystem climber, Subsystem drivetrain, Subsystem shooter, Subsystem collector){
//         Command rotate = new AutoClimb(climber);
//         Command shoot = new AutoShoot(shooter);
//         //158.95
//         Command drive = new AutoDrive(drivetrain, -50); //drive backwards so robot center is on tarmaic vertex
//         Command turnToBall1 = new AutoTurnBasic(drivetrain, 85); // align robot to face cargo       
//         Command driveToCargo = new AutoDrive(drivetrain, 45);
//         ParallelRaceGroup driveAndCollect = new ParallelRaceGroup(driveToCargo, new AutoCollect(collector));
//         Command turnToHub = new AutoTurnBasic(drivetrain, 85); // align robot to face hub  
//         Command driveToHub = new AutoDrive(drivetrain, 30);   
//         // Command turnToBall2 = new AutoTurnBasic(drivetrain, -40);
//         // Command driveToCargo2 = new AutoDrive(drivetrain, 100);
//         // ParallelRaceGroup driveAndCollect2 = new ParallelRaceGroup(driveToCargo2, new AutoCollect(collector));
//         // Command turnToHub = new AutoTurnBasic(drivetrain, -90);
//         // Command driveToHub = new AutoDrive(drivetrain, 30);
//         return new SequentialCommandGroup(rotate, shoot, drive, turnToBall1, driveAndCollect, turnToHub, driveToHub, shoot);
//     }
//     public static Command shootAndCollect2Position1(Subsystem climber, Subsystem drivetrain, Subsystem shooter, Subsystem collector){
//         Command rotate = new AutoClimb(climber);
//         Command shoot = new AutoShoot(shooter);
//         //158.95
//         Command drive = new AutoDrive(drivetrain, -50); //drive backwards so robot center is on tarmaic vertex
//         Command turnToBall1 = new AutoTurnBasic(drivetrain, 85); // align robot to face cargo       
//         Command driveToCargo = new AutoDrive(drivetrain, 45);
//         ParallelRaceGroup driveAndCollect = new ParallelRaceGroup(driveToCargo, new AutoCollect(collector));
//         Command turnToBall2 = new AutoTurnBasic(drivetrain, 85); // align robot to face hub  
//         Command driveToHub = new AutoDrive(drivetrain, 30);
//         // Command turnToBall2 = new AutoTurnBasic(drivetrain, -40);
//         // Command driveToCargo2 = new AutoDrive(drivetrain, 100);
//         // ParallelRaceGroup driveAndCollect2 = new ParallelRaceGroup(driveToCargo2, new AutoCollect(collector));
//         // Command turnToHub = new AutoTurnBasic(drivetrain, -90);
//         // Command driveToHub = new AutoDrive(drivetrain, 30);
//         return new SequentialCommandGroup(rotate, shoot, drive, turnToBall1, driveAndCollect, turnToBall2, driveToHub);
//     }
    
// }
/*

    public static Command shootAndCollectPosition1(Subsystem drivetrain, Subsystem shooter, Subsystem collector){
        Command shoot = new AutoShoot(shooter);
        Command drive = new AutoTurn(drivetrain, - (HUB_TO_VERTEX - HUB_LENGTH - ROBOT_LENGTH/2), 0); //drive backwards so robot center is on tarmaic vertex
        Command turn = new AutoTurn(drivetrain, 0, VERTEX_ANGLE_FROM_TARMAC_SIDE); // align robot to face cargo
        Command driveToCargo = new AutoTurn(drivetrain, 93.90, 0);
        //ParallelRaceGroup driveAndCollect = new ParallelRaceGroup(driveToCargo, new AutoCollect(collector));
        return new SequentialCommandGroup(shoot, drive, turn);
    }
    
    public static Command shootAndCollectPosition1Arc(Subsystem drivetrain, Subsystem shooter, Subsystem collector){
        Command shoot = new AutoShoot(shooter);
        Command drive = new AutoTurn(drivetrain, - (HUB_TO_VERTEX - HUB_LENGTH - ROBOT_LENGTH/2), 0); //drive backwards so robot center is on tarmaic vertex
        Command turn = new AutoTurn(drivetrain, 0, VERTEX_ANGLE_FROM_TARMAC_SIDE); // align robot to face cargo
        Command driveToCargo = new AutoTurn(drivetrain, 93.90, 0);
        //ParallelRaceGroup driveAndCollect = new ParallelRaceGroup(driveToCargo, new AutoCollect(collector));
        return new SequentialCommandGroup(shoot, drive, turn);
    }
    public static Command shootAndCollectPosition2(Subsystem drivetrain, Subsystem shooter, Subsystem collector){
        Command shoot = new AutoShoot(shooter);
        Command drive = new AutoTurn(drivetrain, - (HUB_TO_VERTEX - HUB_LENGTH - ROBOT_LENGTH/2), 0); //drive backwards so robot center is on tarmaic vertex
        Command turn = new AutoTurn(drivetrain, 0, -VERTEX_ANGLE_FROM_TARMAC_SIDE); // align robot to face cargo
        Command driveToCargo = new AutoTurn(drivetrain, 93.90, 0);
        //ParallelRaceGroup driveAndCollect = new ParallelRaceGroup(driveToCargo, new AutoCollect(collector));
        return new SequentialCommandGroup(shoot, drive, turn);
    }
}
*/