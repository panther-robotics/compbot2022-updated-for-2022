
package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.Subsystem;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.OI;
import frc.robot.Robot;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.Shooter.ShotSpeed;

/**Command for managing the shooter. Uses the subsystem {@link Shooter}. */
public class AutoShoot extends CommandBase {
  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
  private final Subsystem shooter;
  private double timeStamp;
  public ShotSpeed shootSpeed;
  public double shotTime;

  public AutoShoot (Subsystem subsystem, ShotSpeed shootSpeed, double shotTime) {
    shooter = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
    this.shootSpeed = shootSpeed;
    this.shotTime = shotTime;
  }

  public AutoShoot(Subsystem subsystem, ShotSpeed shootSpeed) {
    shooter = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
    this.shootSpeed = shootSpeed;
    this.shotTime = 1;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    timeStamp = Timer.getFPGATimestamp();
  }
    
  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    Robot.shooter.shoot(shootSpeed); 
  
    }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    Robot.shooter.shoot(ShotSpeed.OFF);

  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return Timer.getFPGATimestamp() - timeStamp >= shotTime;
  }
}
