// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.Shooter.ShotSpeed;

/**Command for managing the shooter. Uses the subsystem {@link Shooter}. */
public class ShootCommand extends CommandBase {
  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
  private final Shooter shooter;
  public ShotSpeed shotSpeed; //change shotspeed outside of command
  public boolean done;

  public ShootCommand(Shooter subsystem) {
    shooter = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
    
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    done = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    //If right trigger is pressed, fire. Otherwise turn off shooter motors.
    shooter.shoot(shotSpeed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    shooter.shoot(ShotSpeed.OFF);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}