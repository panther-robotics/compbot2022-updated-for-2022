// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;
import frc.robot.subsystems.Collector;

/** An example command that uses an example subsystem. */
public class AutoEject extends CommandBase {
  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
  private final Subsystem collector;
  public boolean done;
  public boolean extend;

  /**
   * Creates a new ExampleCommand.
   *
   * @param subsystem The subsystem used by this command.
   */
  public AutoEject(Subsystem subsystem) {
    collector = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(collector);
    extend = false;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    Robot.collector.retractSolenoid();
    done = false;
    if (extend){
      Robot.collector.extendSolenoid();
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
      Robot.collector.runCollector(.5f);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
      Robot.collector.runCollector(0f);
      Robot.collector.retractSolenoid();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
