package frc.robot.commands;


import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.Subsystem;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.OI;
import frc.robot.Robot;
import frc.robot.subsystems.Collector;

public class AutoCollect extends CommandBase {
    private final Subsystem collector;
    public boolean holdOut = false;

    public AutoCollect(Subsystem subsystem) {
      collector = subsystem;
      // Use addRequirements() here to declare subsystem dependencies.
      addRequirements(collector);
    }
  
    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
      Robot.collector.extendSolenoid();
    }
  
    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
    Robot.collector.runCollector(-1);
    }
  
    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
      Robot.collector.retractSolenoid();
      Robot.collector.runCollector(0.0f);
    }
  
    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
      return false;
    }
  }
  