// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import frc.robot.subsystems.Drivetrain;
import frc.robot.OI;
import frc.robot.Robot;
import edu.wpi.first.wpilibj2.command.CommandBase;


/**
 * Command for managing the drivetrain. Uses the subsysgtem {@link Drivetrain}.
 */
public class DriveCommand extends CommandBase {

  @SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.SingularField" })
  private final Drivetrain drivetrain;
  public boolean done;

  public DriveCommand(Drivetrain subsystem) {
    drivetrain = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    // leftMotors = new
    // SpeedControllerGroup(topLeftMotor,midLeftMotor,bottomLeftMotor);
    // rightMotors = new
    // SpeedControllerGroup(topRightMotor,midRightMotor,bottomRightMotor);
    // drive = new DifferentialDrive(leftMotors, rightMotors);
    done = false;
 
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

      Robot.drivetrain.drive((float)(OI.getLeftStickY()), (float)(OI.getLeftStickX()));
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    drivetrain.drive(0,0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}