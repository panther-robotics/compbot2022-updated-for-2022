package frc.robot.commands.collector;

import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;

public class AutoCollect extends CommandBase {
    private final Subsystem collector;
    
    public boolean holdOut = false;
  
    public AutoCollect(Subsystem subsystem) {
      collector = subsystem;
      // Use addRequirements() here to declare subsystem dependencies.
      addRequirements(collector);
    }
  
    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
      Robot.collector.extendSolenoid();
    }
  
    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
    Robot.collector.runCollector(1);
    }
  
    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
      Robot.collector.retractSolenoid();
    }
  
    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
      return false;
    }
  }
  