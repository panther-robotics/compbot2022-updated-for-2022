// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.climber;

import frc.robot.subsystems.*;
import frc.robot.OI;
import frc.robot.Robot;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.Subsystem;

/**Command for managing climbing. Uses the subsystem {@link Climber}. */
public class AutoClimb extends CommandBase {

    private final Subsystem climber;

    public AutoClimb(Subsystem subsystem){
        climber = subsystem;
        addRequirements(climber);
    }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    // leftMotors = new SpeedControllerGroup(topLeftMotor,midLeftMotor,bottomLeftMotor);
    // rightMotors = new SpeedControllerGroup(topRightMotor,midRightMotor,bottomRightMotor);
    // drive = new DifferentialDrive(leftMotors, rightMotors);
   // climber.push_out();
   Robot.collector.retractSolenoid();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    Robot.climber.rotateClockwise();
   }
   
  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    Robot.climber.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return Robot.climber.getHallSensor();
  }
}
