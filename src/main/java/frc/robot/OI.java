package frc.robot;

import edu.wpi.first.wpilibj.XboxController;

public class OI {
    
    /**Xbox controller instance. */
    public static XboxController xboxDrive;
    /**Default trigger sensitivity. */
    public static double trigger_sensitivity = 0.3;
    /**Default stick deadzones. */
    public static double stick_deadzones = 0.2;
    
    /**Returns the Y value of the left stick.
     * @return Y value of the left stick. Ranges from 0 to 1.
     */
    public static double getLeftStickY() {return xboxDrive.getLeftY();}
    /**Returns the X value of the left stick.
     * @return X value of the left stick. Ranges from 0 to 1.
     */
    public static double getLeftStickX() {return xboxDrive.getLeftX();}
    
    /**Returns the Y value of the right stick.
     * @return Y value of the right stick. Ranges from 0 to 1.
     */
    public static double getRightStickY() {return xboxDrive.getRightY();}
    /**Returns the X value of the right stick. 
     * @return X value of the right stick. Ranges from 0 to 1.
    */
    public static double getRightStickX() {return xboxDrive.getRightX();}

    /**Returns if the left stick is outside of the deadzone.
     * @param zone zone is the distance from the center that the deadzone ends. Ranges from 0 to 1.
     * @return true if the left stick is moved in any direction farther than deadzone size.
    */
    public static boolean getLeftStickOutOfDeadzone(double zone) {return xboxDrive.getLeftY() >= zone || xboxDrive.getLeftY() <= -zone || xboxDrive.getLeftX() >= zone || xboxDrive.getLeftX() <= -zone;}
    /**Returns if the left stick is outside of the deadzone using default size (0.2). 
     * @return true if the left stick is moved in any direction farther than the default deadzone size.
    */
    public static boolean getLeftStickOutOfDeadzone() {return xboxDrive.getLeftY() >= stick_deadzones || xboxDrive.getLeftY() <= -stick_deadzones || xboxDrive.getLeftX() >= stick_deadzones || xboxDrive.getLeftX() <= -stick_deadzones;}
    /**Returns if the right stick is outside of the deadzone.
     * @param zone zone is the distance from the center that the deadzone ends. Ranges from 0 to 1.
     * @return true if the left stick is moved in any direction farther than deadzone size.
    */
    public static boolean getRightStickOutOfDeadzone(double zone) {return xboxDrive.getRightY() >= zone || xboxDrive.getRightY() <= -zone || xboxDrive.getRightX() >= zone || xboxDrive.getRightX() <= -zone;}
    /**Returns if the right stick is outside of the deadzone using default size (0.2). 
     * @return true if the right stick is moved in any direction farther than the default deadzone size.
    */
    public static boolean getRightStickOutOfDeadzone() {return xboxDrive.getRightY() >= stick_deadzones || xboxDrive.getRightY() <= -stick_deadzones || xboxDrive.getRightX() >= stick_deadzones || xboxDrive.getRightX() <= -stick_deadzones;}

    /**Returns state of the left bumper. 
     * @return true if the left bumper is pressed.
    */
    public static boolean getLeftBumper() {return xboxDrive.getLeftBumper();}
    /**Returns state of the right bumper. 
     * @return true if the right bumper is pressed.
    */
    public static boolean getRightBumper() {return xboxDrive.getRightBumper();}

    /**Returns state of the A button. 
     * @return true if the A button is pressed.
    */
    public static boolean getA() {return xboxDrive.getAButton();}
    /**Returns state of the B button. 
     * @return true if the B button is pressed.
    */
    public static boolean getB() {return xboxDrive.getBButton();}
    /**Returns state of the X button. 
     * @return true if the X button is pressed.
    */
    public static boolean getX() {return xboxDrive.getXButton();}
    /**Returns state of the Y button. 
     * @return true if the Y button is pressed.
    */
    public static boolean getY() {return xboxDrive.getYButton();}

    /**Returns state of the start button. 
     * @return true if the start button is pressed.
    */
    public static boolean getStart() {return xboxDrive.getStartButton();}
    /**Returns state of the back button. 
     * @return true if the back button is pressed.
    */
    public static boolean getBack() {return xboxDrive.getBackButton();}

    /**Returns value of the right trigger. Ranges from 0 to 1. 
     * @return the value of the right trigger.
    */
    public static double getRightTrigger() {return xboxDrive.getRightTriggerAxis();}
    /**Returns the value of left trigger. Ranges from 0 to 1. 
     * @return the value of the left trigger.
    */
    public static double getLeftTrigger() {return xboxDrive.getLeftTriggerAxis();}

    /**Returns if the right trigger is pressed using default sensitivity (0.3). 
     * @return true if the trigger is past default sensitivity.
    */
    public static boolean getRightTriggerPressed() {return xboxDrive.getRightTriggerAxis() >= trigger_sensitivity;}
    /**Returns if the right trigger is pressed.
     * @param sensitivity how far the trigger needs to be pressed for it to return true.
     * @return true if the trigger is past the sensitivity.
     */
    public static boolean getRightTriggerPressed(double sensitivity) {return xboxDrive.getRightTriggerAxis() >= sensitivity;}
    /**Returns if the left trigger is pressed using default sensitivity (0.3). 
     * @return true if the trigger is past default sensitivity.
    */
    public static boolean getLeftTriggerPressed() {return xboxDrive.getLeftTriggerAxis() >= trigger_sensitivity;}
    /**Returns if the left trigger is pressed.
     * @param sensitivity how far the trigger needs to be pressed for it to return true.
     * @return true if the trigger is past the sensitivity.
     */
    public static boolean getLeftTriggerPressed(double sensitivity) {return xboxDrive.getLeftTriggerAxis() >= sensitivity;}


}