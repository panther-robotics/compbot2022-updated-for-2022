package frc.robot.sensors;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;
import frc.robot.Robot;


import java.util.Arrays;

public class Pixy2 extends Thread implements IUpdate {

    //Current objective of the pixy
    private Objective OBJECTIVE;
    //The size of the incoming/outgoing packets (the amount of bytes for objects + the standard packet receiving data).
    private int MAX_BYTES = 18;
    private Vector[] vectors = new Vector[Constants.MAX_OBJECTS];
    private Blob[] blobs = new Blob[Constants.MAX_OBJECTS];
    private int totalVectors = 0;
    private int totalBlobs = 0;
    //Packet buffers for sending and receiving
    //Used for setting the purpose of the pixy

    private final String pixyName;
    private boolean running = true;
    private int i = 0;
    private byte[] sending;
    private I2C connection;
    /**
     * Used for setting the objective of Pixy2 camera.
     */
    public enum Objective {
        LINE_TRACKING(12),
        BLOB_RECOGNITION(Blob.FEATURE_LENGTH);

        // used to command solenoid
        public final int value;

        Objective(int value) {
            this.value = value;
        }
    }

    /**
     * Used for setting the brightness of the Pixy2 camera.
     */
    public enum Brightness {
        OFF(0),
        LOW(0x22),
        MEDIUM(0x18),
        HIGH(0xFF);
        public final int value;

        Brightness(int value) {
            this.value = value;
        }
    }

    /**
     * @param port          The port identified on the board (most often, kOnboard except if pixy is plugged into MXP array).
     * @param deviceAddress The I2C address of the pixy2.
     * @param objective     The intention of the pixy device. Can be changed later.
     */

    public Pixy2(I2C.Port port, int deviceAddress, Objective objective, String name) {
        connection = new I2C(port,deviceAddress);
        log("made object");
        //Checks to make sure the pixy is found at the device address
        isConnected();
        // setBrightness(Brightness.MEDIUM);
        this.OBJECTIVE = objective;
        MAX_BYTES = OBJECTIVE.value * Constants.MAX_OBJECTS + 6;
        pixyName = name;
    }

    /**
     * @param intention The desired objective of the Pixy2.
     */
    public void setObjective(Objective intention) {
        OBJECTIVE = intention;
        MAX_BYTES = intention.value * Constants.MAX_OBJECTS + 6;
    }

    /**
     * @return Current objective of the Pixy2.
     */
    public Objective getObjective() {
        return OBJECTIVE;
    }

    /**
     * Captures any current data into the receiving packet.
     */
    public void update() {
        //Checks again to make sure pixy is connected
        // if (isConnected()) {

        //checks to see if it was able to read; false = success
        // If aborted, exits method
        byte[] receivingPacket = new byte[500];
        connection.readOnly(receivingPacket, 18);
        log("tried to receive packet");
        translate(receivingPacket);

        //  }


    }

    /**
     * @return true:connected | false:disconnected
     */
    public boolean isConnected() {
        if (connection.addressOnly()) {
            log("found pixy");
            return true;
        } else {
            log("failed to find pixy");
            return false;
        }
    }
    
    /**
     * Displays the logged information on the SmartDashboard.
     *
     * @param info the data to log.
     */
    private void log(String info) {
        SmartDashboard.putString("pixy", info);
    }

    /**
     * @param bytes      The storage place for the new bytes.
     * @param index      The index of storage to start adding new bytes.
     * @param extraBytes The new bytes to add.
     * @returns The next blank index.
     */
    private int fillBytes(byte[] bytes, int index, int extraBytes) {
        int i;

        if (extraBytes == 0) {
            bytes[index] = (byte) 0 & 0xFF;
            return index + 1;
        }
        for (i = 0; extraBytes != 0; i++, extraBytes >>>= 8) {
            bytes[index + i] = (byte) (extraBytes & 0xFF);
        }
        return i + index;
    }

    /**
     * @param bytes The bytes to be sent, as integer values.
     */

    private void send(int packetSize, int... bytes) {
         sending = new byte[packetSize];

         i = 0;
        for (int bits : bytes)
            i = fillBytes(sending, i, bits);
        connection.writeBulk(sending, packetSize);
        try{
            Thread.sleep(10);
        }
        catch(InterruptedException e) {
            e.printStackTrace();
        }

        log("tried to send packet");
    }

    //Data is sent here to be packaged into an unsigned char array.
    private void translate(byte[] packet) {
        int[] numData = new int[packet.length];
        for (int i = 0; i < packet.length; i++) {
            numData[i] = packet[i];
        }
        
        if ((((numData[1]) & 0xFF) == 0xc1) && ((numData[0]) & 0xFF) == 0xaf) {
            if ((numData[2] & 0xFF) == 0x31) {
                handleLines(Arrays.copyOfRange(packet, 3, packet.length));
                SmartDashboard.putString("Found","line(s)");
            } else if ((numData[2] & 0xFF) == 0x21) {
                handleBlobs(Arrays.copyOfRange(packet, 3, packet.length));
                SmartDashboard.putString("Found","blob(s)");
            } else if ((numData[2]&0xFF) == Pixy2_API.TYPE_RESPONSE_VERSION) {
                SmartDashboard.putString("Found", "Firmware");
                SmartDashboard.putNumber("Pixy2 Firmware Version", combine(packet[10], packet[11]));
            }
               else
                SmartDashboard.putString("Found","nothing");
        } else {
            SmartDashboard.putString("Found", "No Message");
        }
    }

    /**
     * Changes the brightness of the camera (0-255).
     */
    public void setBrightness(Brightness brightness) {
        send(5, Pixy2_API.NO_CHECKSUM_SYNC,
                Pixy2_API.TYPE_REQUEST_BRIGHTNESS,
                1,
                brightness.value);
    }

    public void getVersion() {
        send(4, Pixy2_API.NO_CHECKSUM_SYNC,
                Pixy2_API.TYPE_REQUEST_VERSION,
                0);
    }

    /**
     * Creates new vectors/intersections to be used.
     *
     * @param bits The data line to be interpreted.
     */

    private void handleLines(byte[] bits) {
        //The length of payload

        int length = bits[0];
        totalVectors = 0;
        for (int i = 3; i < length; i += Vector.FEATURE_LENGTH) {
            if (bits[i] == 0x01) {
                //Creates a new vector based off of the data in the packet
                SmartDashboard.putNumber("bits", bits[i]);
                totalVectors++;
                SmartDashboard.putNumber("found line",SmartDashboard.getNumber("found line",0));
                vectors[(i / Vector.FEATURE_LENGTH)] = new Vector(
                        bits[i + Vector.FIRST_X_INDEX],
                        bits[i + Vector.FIRST_Y_INDEX],
                        bits[i + Vector.SECOND_X_INDEX],
                        bits[i + Vector.SECOND_Y_INDEX]);
            }

        }
    }

    private void handleBlobs(byte[] bits) {
        //The length of payload
        int length = bits[0];
        totalBlobs = 0;
        for (int i = 0; i < length; i += Blob.FEATURE_LENGTH) {
                //Creates a new blob based off of the data in the packet
                totalBlobs++;
                blobs[i/Blob.FEATURE_LENGTH] = new Blob(
                        combine(bits[i+Blob.SIGNATURE_INDEX],bits[i+Blob.SIGNATURE_INDEX+1]),
                        combine(bits[i+Blob.X_INDEX],bits[i+Blob.X_INDEX+1]),
                        combine(bits[i+Blob.Y_INDEX],bits[i+Blob.Y_INDEX+1]),
                        combine(bits[i+Blob.WIDTH_INDEX],bits[i+Blob.WIDTH_INDEX+1]),
                        combine(bits[i+Blob.HEIGHT_INDEX],bits[i+Blob.HEIGHT_INDEX+1]));
        }
    }

    public void getMainFeatures() {
        send(6, Pixy2_API.NO_CHECKSUM_SYNC,
                0x30, 0x2, 0x0, 0x7);
    }

    public void getResolution() {
        send(5, Pixy2_API.NO_CHECKSUM_SYNC,
                Pixy2_API.TYPE_REQUEST_RESOLUTION,
                1,
                0);
    }

    public Object[] getObjects() {
        getBlobs();
        update();
        if ((getObjective() == Objective.LINE_TRACKING) && vectors != null) {
            SmartDashboard.putString("returning: ", "lines");
            return Arrays.copyOfRange(vectors, 0, totalVectors);

        }
        else if ((getObjective() == Objective.BLOB_RECOGNITION) && blobs != null) {
            SmartDashboard.putString("returning: " ,"balls");
            return Arrays.copyOfRange(blobs, 0, totalBlobs);
        }
        else {
            SmartDashboard.putString("returning: ", "null");
            return null;
        }
    }

    public void getBlobs() {
        send(6,
                Pixy2_API.NO_CHECKSUM_SYNC,
                0x20,
                0x02,
                3,
                Constants.MAX_OBJECTS);
    }

    private short combine(byte one, byte two) {
        return (short) ((one&0xFF)|(two<<8));
    }

    @Override
    public void run() {
        while (running) {
            if (OBJECTIVE == Objective.LINE_TRACKING) {
                getMainFeatures();
                update();
                
                SmartDashboard.putNumber("vectors length",vectors.length);
                SmartDashboard.putNumber("front facing null", totalVectors);
            } else {
                getBlobs();
                update();
            }

        }

    }

    public void pause() {
        running = false;
    }

    public void open() {
        running = true;
    }


    public String getPixyName() {
        return pixyName;
    }
}
