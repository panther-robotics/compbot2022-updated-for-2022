package frc.robot.sensors;

public class Vector {
	public final double x0;
	public final double x1;
	public final double y0;
	public final double y1;
	public static int FEATURE_LENGTH = 6;
	public static int FIRST_X_INDEX = 2;
	public static int SECOND_X_INDEX = 4;
	public static int FIRST_Y_INDEX = 3;
	public static int SECOND_Y_INDEX = 5;
	public Vector(double x1, double y1, double x2, double y2) {
		this.x0 = x1;
		this.y0 = y1;
		this.x1 = x2;
		this.y1 = y2;
	}

}

