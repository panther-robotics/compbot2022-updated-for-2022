package frc.robot.sensors;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Blob {
	private short x;
	private short y;
	private short width;
	private short height;
	private short signature;
	private boolean wasUpdated;
	private boolean existed;
	public static int SIGNATURE_INDEX = 3;
	public static int X_INDEX = 5;
	public static int Y_INDEX = 7;
	public static int WIDTH_INDEX = 9;
	public static int HEIGHT_INDEX = 11;
	public static int FEATURE_LENGTH = 14;
	public Blob(short signature, short x, short y, short width, short height) {
		SmartDashboard.putNumber("Blob Signature", signature);
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.signature = signature;
	}

	public short getX() {
		return x;
	}

	public short getY() {
		return y;
	}

	public short getWidth() {
		return width;
	}

	public short getHeight() {
		return height;
	}

	public short getSignature() {
		return signature;
	}

	public void setUpdated(boolean value) {wasUpdated = value;}

	public boolean getUpdated() {return wasUpdated;}

	public boolean getExistence() {return existed;}

	public void setExistence(boolean value) {existed = value;}

	public void setX(short x) {this.x = x;}

	public void setHeight(short height) {this.height = height;}

	public void setWidth(short width) {this.width = width;}

	public void setY(short y) {this.y = y;}

	public void setSignature(short signature) {this.signature = signature;}

}