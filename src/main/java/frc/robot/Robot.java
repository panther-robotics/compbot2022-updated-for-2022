// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.cscore.UsbCamera;
import edu.wpi.first.util.datalog.DataLog;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.*;
import frc.robot.subsystems.*;
import frc.robot.commands.autoPaths.MotionMagicPaths;
import frc.robot.commands.climber.ClimbCommand;
import frc.robot.commands.collector.CollectCommand;
import frc.robot.commands.collector.EjectCommand;
import frc.robot.commands.collector.HoldOutCommand;
import frc.robot.commands.drivetrain.DriveCommand;
import frc.robot.commands.drivetrain.LimeLightTurn;
import frc.robot.commands.shooter.ShootCommand;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Shooter.ShotSpeed;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.DataLogManager;
import edu.wpi.first.wpilibj.DriverStation;

/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {

  private Command m_autonomousCommand;
  private RobotContainer m_robotContainer;
  
  public static Drivetrain drivetrain;
  public static Collector collector;
  public static Climber climber;
  public static Shooter shooter;
  public static Vision vision;
  public static Lime_light_good limelight;
  public static XboxController xboxDrive;
  public static float hv = 0;
  public UsbCamera camera;
  public HoldOutCommand holdOut;
  public CollectCommand collect;
  public EjectCommand eject;
  public DriveCommand drive;
  public LimeLightTurn limelightturn;
  public ClimbCommand climb;
  public ShootCommand shoot;

  private int updateSensorsForDisabled = 0; 
  //public static CommandScheduler scheduler;

  /**
   * This function is run when the robot is first started up and should be used for any
   * initialization code.
   */
  @Override
  public void robotInit() {
    // Instantiate our RobotContainer.  This will perform all our button bindings, and put our
    // autonomous chooser on the dashboard.
    DataLogManager.start();
    DriverStation.startDataLog(DataLogManager.getLog());

    OI.xboxDrive = new XboxController(RobotMap.XBOX_PORT);
    m_robotContainer = new RobotContainer();
    drivetrain = new Drivetrain();
    vision = new Vision();
    collector = new Collector();
    shooter = new Shooter();
    climber = new Climber();
    limelight = new Lime_light_good();

    //CameraServer.startAutomaticCapture("top camera", 0);
    LiveWindow.disableAllTelemetry();
    // JoystickButton startButton = new JoystickButton(xboxDrive, 8);
    // startButton.whenPressed(new ExtendLift(lift));
    // JoystickButton backButton = new JoystickButton(xboxDrive, 7);
    // backButton.whenPressed(new RetractLift(lift));
    CommandScheduler.getInstance().registerSubsystem(drivetrain);
    CommandScheduler.getInstance().registerSubsystem(collector);
    CommandScheduler.getInstance().registerSubsystem(shooter);
    CommandScheduler.getInstance().registerSubsystem(climber);
    CommandScheduler.getInstance().registerSubsystem(limelight);

    //SmartDashboard.putNumber("autoDriveDistance", 0);
    //SmartDashboard.putNumber("autoTurnDegrees", 0);
    
  }

  /**
   * This function is called every robot packet, no matter the mode. Use this for items like
   * diagnostics that you want ran during disabled, autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before LiveWindow and
   * SmartDashboard integrated updating.
   */

  @Override
  public void robotPeriodic() {
    // Runs the Scheduler.  This is responsible for polling buttons, adding newly-scheduled
    // commands, running already-scheduled commands, removing finished or interrupted commands,
    // and running subsystem periodic() methods.  This must be called from the robot's periodic
    // block in order for anything in the Command-based framework to work.
    CommandScheduler.getInstance().run();
    //SmartDashboard.putNumber("climber encoder: ", climber.climb_motor.getSelectedSensorPosition());
    if (limelight.x!=0){
      SmartDashboard.putBoolean("Limelight detect", true);
    }else{
      SmartDashboard.putBoolean("Limelight detect", false);
    }
  }


  /** This function is called once each time the robot enters Disabled mode. */
  @Override
  public void disabledInit() {
  }

  @Override
  public void disabledPeriodic() {
    if(updateSensorsForDisabled == 10){
      Robot.drivetrain.initMotionMagic();
      teleopInit();
      updateSensorsForDisabled = 0; 
    }
    updateSensorsForDisabled++; 
  }
  
  /** This autonomous runs the autonomous command selected by your {@link RobotContainer} class. */
  @Override
  public void autonomousInit() {
   
   if (m_robotContainer.getAutonomousCommand()==1){
      m_autonomousCommand = MotionMagicPaths.shootAndCrossLine(drivetrain, shooter, climber);
    }else if (m_robotContainer.getAutonomousCommand()==2){
      m_autonomousCommand = MotionMagicPaths.twoBallPosition1(climber, drivetrain, shooter, collector);
    }else if (m_robotContainer.getAutonomousCommand()==3){
       m_autonomousCommand = MotionMagicPaths.diffThreeballPosition0(climber, drivetrain, shooter, collector);
    }else {
       m_autonomousCommand = MotionMagicPaths.shootAndCrossLine(drivetrain, shooter, climber);
    }

    System.out.println("autonomous starting");
    drivetrain.initTeleop();
    drivetrain.initMotionMagic();
    // schedule the autonomous command (example)
    m_autonomousCommand.schedule();
  }
  
  /** This function is called periodically during autonomous. */
  @Override
  public void autonomousPeriodic() {
    
  }

  @Override
  public void teleopInit() {
    // This makes sure that the autonomous stops running when
    // teleop starts running. If you want the autonomous to
    // continue until interrupted by another command, remove
    // this line or comment it out.
    drivetrain.initTeleop();
    if (m_autonomousCommand != null) {
      m_autonomousCommand.cancel();
    }
    collect = new CollectCommand(collector);
    eject = new EjectCommand(collector);
    holdOut = new HoldOutCommand(collector);
    drive = new DriveCommand(drivetrain);
    limelightturn = new LimeLightTurn(drivetrain);
    climb = new ClimbCommand(climber);
    shoot = new ShootCommand(shooter);

  }

  /** This function is called periodically during operator control. */
  @Override
  public void teleopPeriodic() {
    if (OI.getLeftTriggerPressed()){
      collect.schedule();
    }else if (OI.getLeftBumper()){
      eject.extend = true;
      eject.schedule();
    }else if (OI.getBack()){
      holdOut.schedule();
    }else if (OI.getRightTriggerPressed()){
      eject.extend = false;
      eject.schedule();
    }else{
      eject.done = true;
      collect.done = true;
    }
    if (OI.getRightBumper()){
      drive.done=true;
      limelightturn.schedule();
    }else{
      limelightturn.done = true;
      drive.schedule();
    }
    if (OI.getY()){
      climb.direction = true;
      climb.schedule();
    }else if (OI.getA()){
      climb.direction = false;
      climb.schedule();
    }else{
      climb.done = true;
    }
    if (OI.getRightTriggerPressed()){
      shoot.shotSpeed = ShotSpeed.MID;
      shoot.schedule();
    }else if (OI.getLeftBumper()){
      shoot.shotSpeed = ShotSpeed.REVERSE;
      shoot.schedule();
    }else{
      shoot.done = true;
    }
  }

  @Override
  public void testInit() {
    // Cancels all running commands at the start of test mode.
    CommandScheduler.getInstance().cancelAll();
  }

  /** This function is called periodically during test mode. */
  @Override
  public void testPeriodic() {}
}
