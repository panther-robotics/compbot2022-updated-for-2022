// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.TalonSRXControlMode;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;

/**The collector subsystem. Implements functionality for the collector {@link VictorSPX} and collector {@link DoubleSolenoid}. */
public class Collector extends SubsystemBase {

  /**{@link VictorSPX} motor to run the collector's wheels. */
  public TalonSRX leftCollectMotor;
  public TalonSRX rightCollectMotor;
  /**{@link DoubleSolenoid} to extend and retract collector. */
  public DoubleSolenoid collector;
  public int timed;

  public Collector() {

    leftCollectMotor = new TalonSRX(RobotMap.leftCollectMotor);
    rightCollectMotor = new TalonSRX(RobotMap.rightCollectMotor);
    collector = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, RobotMap.CollectorSolenoidLeft, RobotMap.CollectorSolenoidRight);

    rightCollectMotor.set(TalonSRXControlMode.PercentOutput, 0);
    leftCollectMotor.set(TalonSRXControlMode.PercentOutput, 0);


  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }

  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }
  
  /**Extends the collector forwards.*/
  public void extendSolenoid() {
    collector.set(DoubleSolenoid.Value.kForward);
  }

  /**Retracts the collector backwards. */
  public void retractSolenoid() {
    collector.set(DoubleSolenoid.Value.kReverse);
  }
  
  /**Changes collector motor speed.
   * @param speed the speed to set motor to.
   */
  public void runCollector(float speed) {
    leftCollectMotor.set(TalonSRXControlMode.PercentOutput, speed);
    rightCollectMotor.set(TalonSRXControlMode.PercentOutput, speed);

  }
}