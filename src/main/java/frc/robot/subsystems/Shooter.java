// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.TalonFXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.TalonSRXControlMode;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;

/**
 * The shooter subsystem. Implements functionality for the left and right
 * shooter {@link TalonSRX}, as well as the shooter's trigger {@link TalonSRX}.
 */
public class Shooter extends SubsystemBase {

  /** Left shooter {@link TalonSRX}. */
  public TalonFX leftShooter;
  /** Right shooter {@link TalonSRX}. */
  public TalonFX rightShooter;
  /** Shooter trigger {@link TalonSRX}. */
  public TalonSRX trigger;
  /** Shooter {@link TalonSRX} to counter backspin. */
  public TalonFX backspinMotor;

  /** Speed for backspinMotor to move at. */
  public double backSpinSpeed;
  /** Speed for leftShooter and rightShooter to move at. */
  public double shootSpeed;
  /** How long to wait before moving the ball to leftShooter and rightShooter. */
  public int waitTicks;


  public enum ShotSpeed {
    REVERSE(-0.25), OFF(0), LOW(0.7), MID(0.77), HIGH(0.77);

    private double speed;
    private ShotSpeed(double speed){
      this.speed = speed; 
    }

    private ShotSpeed(){
      this.speed = 0; 
    }

    public double getSpeed(){
      return speed; 
    }
  };

  public Shooter() {

    leftShooter = new TalonFX(RobotMap.shooterLeftMotor);
    rightShooter = new TalonFX(RobotMap.shooterRightMotor);
    trigger = new TalonSRX(RobotMap.trigger);
    backspinMotor = new TalonFX(RobotMap.BackSpinMotor);
    leftShooter.setNeutralMode(NeutralMode.Coast);
    trigger.setNeutralMode(NeutralMode.Brake);
    rightShooter.setNeutralMode(NeutralMode.Coast);
    leftShooter.configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor,0,0);

    waitTicks = 20;

  }

  /**Sets the shooter's motors to speed, or in case of the trigger, {@code -speed * 0.3}. c
   * @param speed the speed to set the motors to.
   * @param velocity the encoder velocity the motors need to reach before shooting.
   */
  public void shoot(ShotSpeed shotSpeed){
    shootSpeed = shotSpeed.getSpeed(); //0.75
    backSpinSpeed = 1;
    if(shootSpeed == 0){
      waitTicks = 20;
      trigger.set(TalonSRXControlMode.PercentOutput, 0);
    }
    backspinMotor.set(TalonFXControlMode.PercentOutput, -shootSpeed*10);
    rightShooter.set(TalonFXControlMode.PercentOutput, -shootSpeed);
    leftShooter.set(TalonFXControlMode.PercentOutput, shootSpeed);
    switch(shotSpeed) {
      case REVERSE: //if shooter is going in reverse
        trigger.set(TalonSRXControlMode.PercentOutput, 1);
        break;
      default: //if shooter is not going in reverse
        if(waitTicks <= 0){ //wait required wait ticks before powering motor
          trigger.set(TalonSRXControlMode.PercentOutput, -1);
        }
        break;
    }
    waitTicks--;
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }

  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }
}