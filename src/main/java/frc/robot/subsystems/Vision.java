package frc.robot.subsystems;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.sensors.IUpdate;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.sensors.Blob;
import frc.robot.sensors.Vector;
import frc.robot.sensors.Pixy2;
import edu.wpi.first.wpilibj.DriverStation;

/**The vision subsystem. Implements functionality for the {@link Pixy2}. */
public class Vision extends SubsystemBase{
    /*
    public Pixy2 ballfinder;
    private Blob[] balls = new Blob[Constants.MAX_OBJECTS];
    //private Blob[] colorBalls = new Blob[Constants.MAX_OBJECTS];
    private int x = 0;
    public Blob mainball;
    public boolean hasBall;
    private double currentWidth = 0;

    public Vision() {
        ballfinder = new Pixy2(I2C.Port.kOnboard, RobotMap.Pixy2_Ball_Collector, Pixy2.Objective.BLOB_RECOGNITION, "Ball Collector");
    }
    @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }

  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }


  public double checkPixies() {

    if (ballfinder.getObjective() == Pixy2.Objective.BLOB_RECOGNITION) {
        
        setBlobs(ballfinder);
        if (mainball!=null){
            return 160-mainball.getX();

        } else {
            return 0;
        }
        
    } else {
        return 0;
    }

  }
    public void setBlobs(Pixy2 pixy) {
        balls = (Blob[]) ballfinder.getObjects();
        currentWidth = 0;
        
        if (!(balls == null)) {
            if (balls.length > 0) {
                for (int i = 0; i < balls.length; i++) {
                    System.out.println(DriverStation.getAlliance());
                    SmartDashboard.putNumber("first ball width", balls[i].getWidth());
                    SmartDashboard.putNumber("first ball height", balls[i].getHeight());
                    SmartDashboard.putNumber("first ball X", balls[i].getX());
                    SmartDashboard.putNumber("first ball Y", balls[i].getY());
                    if (DriverStation.getAlliance().equals(DriverStation.Alliance.Blue) && balls[i] != null) {                        
                        if (balls[i].getSignature() == 2 && balls[i].getHeight() * 2 > balls[i].getWidth() && balls[i].getWidth() > currentWidth) {
                            currentWidth = balls[i].getWidth();
                            mainball = balls[i];
                            hasBall = true;
                            SmartDashboard.putString("Team", "Blue");
                        }
                    } else if (DriverStation.getAlliance().equals(DriverStation.Alliance.Red) && balls[i] != null) {
                        
                        if (balls[i].getSignature() == 1 && balls[i].getHeight() * 2 > balls[i].getWidth() && balls[i].getWidth() > currentWidth) {
                            currentWidth = balls[i].getWidth();
                            mainball = balls[i];
                            hasBall = true;
                            SmartDashboard.putString("Team", "Red");
                            SmartDashboard.putNumber("Object", i);
                        }else{
                            //System.out.println("Problem");
                            mainball=null;
                            hasBall=false;
                        }
                    /*} else if (balls[i]!=null) {
                       
                        if (balls[i].getHeight()*2>balls[i].getWidth()) {
                            SmartDashboard.putString("Ball or no", "I think it is a ball");
                        } else {
                            SmartDashboard.putString("Ball or no", "I think it is not a ball");
                        }
                        SmartDashboard.putNumber("first ball X", balls[i].getX());
                        SmartDashboard.putNumber("first ball Y", balls[i].getY());
                        mainball=balls[i];
                        hasBall=true;
                    } else {
                        SmartDashboard.putString("Ball or no", "No objects detected");
                        mainball = null;
                        hasBall = false;
                    }
                }
            }
        }
    }
    */
}
