// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Robot;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.Timer;
import frc.robot.RobotMap;
import frc.robot.util.PositionHeading;
import edu.wpi.first.math.controller.PIDController;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.FollowerType;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.RemoteFeedbackDevice;
import com.ctre.phoenix.motorcontrol.RemoteSensorSource;
import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.TalonFXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonFXConfiguration;
import com.ctre.phoenix.motorcontrol.can.TalonFXPIDSetConfiguration;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.sensors.PigeonIMU;
import com.ctre.phoenix.sensors.WPI_PigeonIMU;

import edu.wpi.first.math.trajectory.TrajectoryUtil;

/**
 * The drivetrain subsystem. Implements functionality for the
 * {@link WPI_TalonFX} motors, {@link PigeonIMU}, and {@link PIDController}.
 */
public class Drivetrain extends SubsystemBase {

  /** Front left {@link WPI_TalonFX}. */
  public WPI_TalonFX frontLeftMotor;
  /** Back left {@link WPI_TalonFX}. */
  public WPI_TalonFX backLeftMotor;
  /** Front right {@link WPI_TalonFX}. */
  public WPI_TalonFX frontRightMotor;
  /** Back right {@link WPI_TalonFX}. */
  public WPI_TalonFX backRightMotor;
  /**
   * A {@link PigeonIMU} gyro used to help with keeping straight and turning
   * precisely.
   */
  public WPI_PigeonIMU pigeonIMU;
  public PIDController pid;
  public static boolean driving;
  public DifferentialDrive differentialDriveBase;
  
  private PositionHeading positionHeading;
  private double timeOffset;
  private double lastTimePositionUpdated;

  public final static double TICKS_PER_INCH = 1047.8667194;
  public final static double TICKS_PER_DEGREE = 22.7555556;
  public float difference;

  private final static double KF_LINEAR_DRIVE = 1023.0/18000.0;
  private final static double KF_DIFFERENTIAL_TURNING = 1023.0/(6000.0/40.0); 

  private double printValues = 0; 

  double[] gyroTest = new double[3];

  public Drivetrain() {
    pigeonIMU = new WPI_PigeonIMU(RobotMap.PigeonIMU);
    frontLeftMotor = new WPI_TalonFX(RobotMap.FRONT_LEFT_MOTOR);
    backLeftMotor = new WPI_TalonFX(RobotMap.BACK_LEFT_MOTOR);
    frontRightMotor = new WPI_TalonFX(RobotMap.FRONT_RIGHT_MOTOR);
    backRightMotor = new WPI_TalonFX(RobotMap.BACK_RIGHT_MOTOR);
    differentialDriveBase = new DifferentialDrive(frontLeftMotor, frontRightMotor);
    positionHeading = new PositionHeading();
 
    
    backRightMotor.setInverted(false);
    frontRightMotor.setInverted(false);
    frontLeftMotor.setInverted(true);
    backLeftMotor.setInverted(true);
    
    frontLeftMotor.setNeutralMode(NeutralMode.Brake);
    backLeftMotor.setNeutralMode(NeutralMode.Brake);
    frontRightMotor.setNeutralMode(NeutralMode.Brake);
    backRightMotor.setNeutralMode(NeutralMode.Brake);

    SmartDashboard.putNumber("kP0", 0.0);
    SmartDashboard.putNumber("kI0", 0.00);
    SmartDashboard.putNumber("kD0", 0);
    SmartDashboard.putNumber("kP1", 0.0);
    SmartDashboard.putNumber("kI1", 0.00);
    SmartDashboard.putNumber("kD1", 0);
    SmartDashboard.putNumber("kF0", 0);
    SmartDashboard.putNumber("kF1", 0);
    SmartDashboard.putNumber("Distance", 0);
    SmartDashboard.putNumber("autoHeading", 0);

    configureTalons();

  }
  
  public void zeroSensors() {
    frontRightMotor.getSensorCollection().setIntegratedSensorPosition(0, 0);
    frontLeftMotor.getSensorCollection().setIntegratedSensorPosition(0,0);
    frontRightMotor.setSelectedSensorPosition(0);
    frontLeftMotor.setSelectedSensorPosition(0);
    frontRightMotor.getSensorCollection().setIntegratedSensorPosition(0, 0);
    pigeonIMU.setYaw(0);
    SmartDashboard.putNumber("Distance", 0);
  }

  public void autoDriveInit(){
    TalonFXConfiguration masterConfiguration = new TalonFXConfiguration(); 
    
    frontRightMotor.configSelectedFeedbackSensor(TalonFXFeedbackDevice.RemoteSensor1, 1, 0);

    frontLeftMotor.configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor, 0, 0);
    masterConfiguration.remoteFilter0.remoteSensorDeviceID = RobotMap.FRONT_LEFT_MOTOR; 
    masterConfiguration.remoteFilter0.remoteSensorSource = RemoteSensorSource.TalonFX_SelectedSensor;
    masterConfiguration.remoteFilter1.remoteSensorDeviceID = RobotMap.PigeonIMU;
    masterConfiguration.remoteFilter1.remoteSensorSource = RemoteSensorSource.Pigeon_Yaw; 
    
    masterConfiguration.sum0Term = TalonFXFeedbackDevice.IntegratedSensor.toFeedbackDevice(); 
    masterConfiguration.sum1Term = TalonFXFeedbackDevice.RemoteSensor0.toFeedbackDevice(); 
    
    masterConfiguration.primaryPID.selectedFeedbackSensor = TalonFXFeedbackDevice.SensorSum.toFeedbackDevice(); 
    masterConfiguration.primaryPID.selectedFeedbackCoefficient = 0.5;

    masterConfiguration.auxiliaryPID.selectedFeedbackSensor = TalonFXFeedbackDevice.RemoteSensor1.toFeedbackDevice();



    frontRightMotor.configAllSettings(masterConfiguration);
    frontRightMotor.getSensorCollection().setIntegratedSensorPosition(0, 0);
    frontLeftMotor.getSensorCollection().setIntegratedSensorPosition(0,0);
    frontRightMotor.setSelectedSensorPosition(0);
    frontLeftMotor.setSelectedSensorPosition(0);
    
    frontLeftMotor.config_kP(0, SmartDashboard.getNumber("kP0", 0));
    frontLeftMotor.config_kI(0, SmartDashboard.getNumber("kI0", 0));
    frontLeftMotor.config_kD(0, SmartDashboard.getNumber("kD0", 0));
    frontRightMotor.config_kP(0, SmartDashboard.getNumber("kP0", 0));
    frontRightMotor.config_kI(0, SmartDashboard.getNumber("kI0", 0));
    frontRightMotor.config_kD(0, SmartDashboard.getNumber("kD0", 0));
    frontLeftMotor.config_kF(0, KF_LINEAR_DRIVE);
    frontLeftMotor.config_kF(1, 0);
    frontRightMotor.config_kF(0, KF_LINEAR_DRIVE);
    frontRightMotor.config_kF(1, 0);
    frontRightMotor.selectProfileSlot(1, 1);
    frontLeftMotor.selectProfileSlot(1, 1);
    frontRightMotor.selectProfileSlot(0, 0);
    frontLeftMotor.selectProfileSlot(0, 0);
    frontLeftMotor.setNeutralMode(NeutralMode.Brake);
    backLeftMotor.setNeutralMode(NeutralMode.Brake);
    frontRightMotor.setNeutralMode(NeutralMode.Brake);
    backRightMotor.setNeutralMode(NeutralMode.Brake);
    initMotionMagic();
  }

  public boolean autoDrive(double distance) {
    frontRightMotor.set(ControlMode.MotionMagic, distance * TICKS_PER_INCH);
    frontLeftMotor.follow(frontRightMotor, FollowerType.AuxOutput1);
    SmartDashboard.putNumber("autodrive target", distance );
    return frontRightMotor.getSelectedSensorPosition() >= ((distance * TICKS_PER_INCH) - 1000) 
      && frontRightMotor.getSelectedSensorPosition() <= ((distance * TICKS_PER_INCH) + 1000);
  }

  public void autoTurnInit(){
//feedback values
frontLeftMotor.config_kP(1, SmartDashboard.getNumber("kP1", 0));
frontLeftMotor.config_kI(1, SmartDashboard.getNumber("kI1", 0));
frontLeftMotor.config_kD(1, SmartDashboard.getNumber("kD1", 0));
frontRightMotor.config_kP(1, SmartDashboard.getNumber("kP1", 0));
frontRightMotor.config_kI(1, SmartDashboard.getNumber("kI1", 0));
frontRightMotor.config_kD(1, SmartDashboard.getNumber("kD1", 0));
frontRightMotor.config_kF(0, 0);
frontLeftMotor.config_kF(0, 0);

frontRightMotor.selectProfileSlot(1, 1); //SLOT 2 FOR PRIMARY PID on right (negative kf turn)
frontLeftMotor.selectProfileSlot(1, 1); //slot 1 for primary pid on left (positive kf turn)
//set master to both targets
//set the pid 0 sensor to the pigeon (remote sensor 1)
pigeonIMU.setYaw(0);
frontRightMotor.configSelectedFeedbackSensor(RemoteFeedbackDevice.RemoteSensor1.getFeedbackDevice(), 0, 0);
frontLeftMotor.configSelectedFeedbackSensor(RemoteFeedbackDevice.RemoteSensor1.getFeedbackDevice(), 0, 0);
frontRightMotor.configSelectedFeedbackCoefficient(1, 0, 0);
frontLeftMotor.configSelectedFeedbackCoefficient(1, 0, 0);
frontRightMotor.setSelectedSensorPosition(0, 0, 0);
frontLeftMotor.setSelectedSensorPosition(0, 0, 0);

initMotionMagic();
}

  public boolean autoTurn(double heading) {
    
    frontLeftMotor.config_kP(1, 0.34);
    frontLeftMotor.config_kI(1, 0.0004);
    frontLeftMotor.config_kD(1, 0);
    frontLeftMotor.config_kF(1, 0);
    frontRightMotor.config_kP(1, 0.34);
    frontRightMotor.config_kI(1, 0.0004);
    
    frontRightMotor.config_kD(1, 0);
    frontRightMotor.config_kF(1, 0);
    TalonFXPIDSetConfiguration config = new TalonFXPIDSetConfiguration(); 
    frontRightMotor.getPIDConfigs(config);

    frontLeftMotor.follow(frontRightMotor, FollowerType.AuxOutput1);
    frontRightMotor.set(ControlMode.MotionMagic, 0, DemandType.AuxPID, heading*TICKS_PER_DEGREE);

    return pigeonIMU.getYaw() >= (heading - 2) && pigeonIMU.getYaw() <= (heading + 2);
  }

  public void initMotionMagic() {
    frontLeftMotor.configNominalOutputForward(0, 0);
    frontLeftMotor.configNominalOutputReverse(0, 0);
    frontLeftMotor.configPeakOutputForward(1, 0);
    frontLeftMotor.configPeakOutputReverse(-1, 0);
    frontRightMotor.configNominalOutputForward(0, 0);
    frontRightMotor.configNominalOutputReverse(0, 0);
    frontRightMotor.configPeakOutputForward(1, 0);
    frontRightMotor.configPeakOutputReverse(-1, 0);
    

    frontLeftMotor.configMotionAcceleration(12 * TICKS_PER_INCH, 0); //was at 8 
    frontLeftMotor.configMotionCruiseVelocity(15 * TICKS_PER_INCH, 0);
    frontRightMotor.configMotionAcceleration(12 * TICKS_PER_INCH, 0);
    frontRightMotor.configMotionCruiseVelocity(15 * TICKS_PER_INCH, 0);
    frontRightMotor.setSelectedSensorPosition(0);
    frontLeftMotor.setSelectedSensorPosition(0);
    pigeonIMU.setYaw(0);
  }

  public void initTeleop() {
    backLeftMotor.follow(frontLeftMotor, FollowerType.PercentOutput);
    backRightMotor.follow(frontRightMotor, FollowerType.PercentOutput);
    frontLeftMotor.set(ControlMode.PercentOutput, 0);
    frontRightMotor.set(ControlMode.PercentOutput, 0);
    SupplyCurrentLimitConfiguration supplyLimit = new SupplyCurrentLimitConfiguration();
    supplyLimit.currentLimit = 80; 
    supplyLimit.enable = true; 
    
    frontLeftMotor.configSupplyCurrentLimit(supplyLimit);
    backLeftMotor.configSupplyCurrentLimit(supplyLimit);
    frontRightMotor.configSupplyCurrentLimit(supplyLimit);
    backRightMotor.configSupplyCurrentLimit(supplyLimit);
    
  }

  @Override
  public void periodic() {
    differentialDriveBase.feedWatchdog();
    if(printValues == 10){
      System.out.println("front right motor speed : "+frontRightMotor.get() + " Current: "+frontRightMotor.getSupplyCurrent() + 
      "\n front left motor speed : "+frontLeftMotor.get() + " Current: "+frontLeftMotor.getSupplyCurrent());
      printValues = 0; 
    } else{
      printValues++;
    }
    //SmartDashboard.putNumber("pigeon heading", pigeonIMU.getYaw());
    
  }

 

  public void start() {
    timeOffset = Timer.getFPGATimestamp();
    lastTimePositionUpdated = timeOffset;
  }

  private double getSpeed() {
    return 0; //frontRightMotor.getSensorCollection().getIntegratedSensorVelocity() * TICKS_PER_INCH * 10;
  }

  public void drive(float y, float z) {
    y=-y;
    //z = (y < -0.3) ? -z : z; 
    differentialDriveBase.arcadeDrive(y, z, true);
    
     SmartDashboard.putNumber("Y", y);
     SmartDashboard.putNumber("Z", z);

  }

  public boolean autoAim(){
    if (Robot.limelight.x > 1 && Robot.limelight.x < 3){
      return true;
    }
    if (Robot.limelight.x > 3 && Robot.limelight.x !=0){
      difference = (float) ((((Robot.limelight.x - 3)/18)*.65)+.35);
      drive(0, difference);
    }else if (Robot.limelight.x < 1 && Robot.limelight.x != 0){
      difference = (float) ((((Robot.limelight.x-1)/22)*.65)-.35);

      drive(0, difference);
    }else{
      drive(0, 0);
    }
    return false;
  }
  
  public void configureTalons(){
    
    TalonFXConfiguration followerConfiguration = new TalonFXConfiguration(); 

    followerConfiguration.remoteFilter1.remoteSensorDeviceID = RobotMap.PigeonIMU;
    followerConfiguration.remoteFilter1.remoteSensorSource = RemoteSensorSource.Pigeon_Yaw; 
    frontLeftMotor.configAllSettings(followerConfiguration);

    frontLeftMotor.configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor, 0, 0);
    frontLeftMotor.configSelectedFeedbackSensor(TalonFXFeedbackDevice.RemoteSensor1, 1, 0);

    TalonFXConfiguration masterConfiguration = new TalonFXConfiguration(); 

    frontRightMotor.configSelectedFeedbackSensor(TalonFXFeedbackDevice.RemoteSensor1, 1, 0);

    masterConfiguration.remoteFilter0.remoteSensorDeviceID = RobotMap.FRONT_LEFT_MOTOR; 
    masterConfiguration.remoteFilter0.remoteSensorSource = RemoteSensorSource.TalonFX_SelectedSensor;
    masterConfiguration.remoteFilter1.remoteSensorDeviceID = RobotMap.PigeonIMU;
    masterConfiguration.remoteFilter1.remoteSensorSource = RemoteSensorSource.Pigeon_Yaw; 
    
    masterConfiguration.sum0Term = TalonFXFeedbackDevice.IntegratedSensor.toFeedbackDevice(); 
    masterConfiguration.sum1Term = TalonFXFeedbackDevice.RemoteSensor0.toFeedbackDevice(); 
    
    masterConfiguration.primaryPID.selectedFeedbackSensor = TalonFXFeedbackDevice.SensorSum.toFeedbackDevice(); 
    masterConfiguration.primaryPID.selectedFeedbackCoefficient = 0.5; 
    masterConfiguration.auxiliaryPID.selectedFeedbackSensor = TalonFXFeedbackDevice.RemoteSensor1.toFeedbackDevice();

    frontRightMotor.configAllSettings(masterConfiguration);

    frontLeftMotor.config_kF(0, 0);
    frontLeftMotor.config_kF(1, 0);
    frontRightMotor.config_kF(0, 0);
    frontRightMotor.config_kF(1, 0);

    frontLeftMotor.setNeutralMode(NeutralMode.Brake);
    backLeftMotor.setNeutralMode(NeutralMode.Brake);
    frontRightMotor.setNeutralMode(NeutralMode.Brake);
    backRightMotor.setNeutralMode(NeutralMode.Brake);
  }


  /**
   * turns the robot to a desired heading
   * @param heading the desired heading in degrees (relative)
   * @return if the pigeon heading is within 1 degree of the target heading 
   */
  public boolean autoTurnBasic(double heading) {
    double scalingValue = 0.009;
    double minDrive = 0.35;
    double maxDrive = 0.95;
    double driveValue = (heading - pigeonIMU.getYaw()) * scalingValue; 
    
    if(driveValue > maxDrive || driveValue < -maxDrive) {
      driveValue = (driveValue>0) ? maxDrive : -maxDrive;
    }
    if(driveValue < minDrive && driveValue > -minDrive ){
      driveValue = (driveValue > 0) ? minDrive : -minDrive;
    }
    SmartDashboard.putNumber("drive value", driveValue);
    drive(0.0f, -(float)driveValue);
    return (pigeonIMU.getYaw() < heading + 1 && pigeonIMU.getYaw() > heading -1);
  }
  public boolean newAutoTurnBasic(double heading){
    double error = heading - pigeonIMU.getYaw();
    double absError = Math.abs(error);
    double driveValue = 0;
    double scalingValue;
    double _10DegreeTurn = 0.34;
    double _30DegreeTurn = 0.45;
    double _60DegreeTurn = 0.6;
    double _90DegreeTurn = 0.7;
    double _135DegreeTurn = 0.8;
    double tempDrive = 0.8;
    double maxDrive = 0.8;
    if(absError < 10){
      driveValue = (error > 0) ? _10DegreeTurn : -_10DegreeTurn;
    } else if(absError < 30){
      scalingValue = (_30DegreeTurn - _10DegreeTurn) / (30-10);
      tempDrive = _10DegreeTurn + (scalingValue* (absError - 10));
      driveValue = (error > 0) ? tempDrive: -tempDrive;
    } else if(absError < 60){
      scalingValue = (_60DegreeTurn - _30DegreeTurn) / (60-30);
      tempDrive = _30DegreeTurn + (scalingValue* (absError - 30));
      driveValue = (error > 0) ? tempDrive: -tempDrive;
    } else if(absError < 90){
      scalingValue = (_90DegreeTurn - _60DegreeTurn) / (90-60);
      tempDrive = _60DegreeTurn + (scalingValue* (absError - 60));
      driveValue = (error > 0) ? tempDrive: -tempDrive;
    } else if(absError < 135){
      scalingValue = (_135DegreeTurn - _90DegreeTurn) / (135-90);
      tempDrive = _90DegreeTurn + (scalingValue* (absError - 90));
      driveValue = (error > 0) ? tempDrive: -tempDrive;
    } else{
      driveValue = (error > 0) ? maxDrive: -maxDrive;
    }
    drive(0.0f, -(float)driveValue);
    return (pigeonIMU.getYaw() < heading + 1 && pigeonIMU.getYaw() > heading -1);
  }
}