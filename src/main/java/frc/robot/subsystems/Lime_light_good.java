// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

public class Lime_light_good extends SubsystemBase {
   NetworkTable table = NetworkTableInstance.getDefault().getTable("limelight");
   public double x;
   public double y;
   public double area;
   public double store;

   public Lime_light_good() {
    table.getEntry("ledMode").setNumber(3);
    table.getEntry("camMode").setNumber(0);
    table.getEntry("pipeline").setNumber(0);
   }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    //read values periodically

    NetworkTableEntry tx = table.getEntry("tx");
    NetworkTableEntry ty = table.getEntry("ty");
    NetworkTableEntry ta = table.getEntry("ta");
    x = tx.getDouble(0.0);
    // if (x==0){
    //     x = store;
    // }
    // store = x;
    y = ty.getDouble(0.0);
    area = ta.getDouble(0.0);
      
    //post to smart dashboard periodically
    SmartDashboard.putNumber("LimelightX", x);
    SmartDashboard.putNumber("LimelightY", y);
    SmartDashboard.putNumber("LimelightArea", area);
    
    
  }

  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }
}
