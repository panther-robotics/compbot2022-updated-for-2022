package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.revrobotics.CANSparkMax;


import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.DigitalInput;
import frc.robot.RobotMap;
import edu.wpi.first.wpilibj.DoubleSolenoid;

/**The climber subsystem. Implements functionality for the climb {@link CANSparkMax} and climb {@link DoubleSolenoid}. */
public class Climber extends SubsystemBase {

    /**Climber {@link CANSparkMax}. Used to raise and lower the climber. */
    public TalonFX leftclimb_motor;
    public TalonFX rightclimb_motor;
    /**Climber {@link DoubleSolenoid}. Used to push the climber away and pull it back. */
  
  
    public int direction;
    public double distanceTicks;
    public boolean distanceTicksReliable;
    public final double MAX_HEIGHT;
    public final double MIN_HEIGHT;
    public double initialEncoderValue;
    public double encoderValue;
    public DigitalInput climbhallEffect;



    public Climber() {
        //Assigning motors and setting speed
        leftclimb_motor = new TalonFX(RobotMap.LeftClimberMotor);
        rightclimb_motor = new TalonFX(RobotMap.RightClimberMotor);
        leftclimb_motor.setNeutralMode(NeutralMode.Brake);
        rightclimb_motor.setNeutralMode(NeutralMode.Brake);

        //Assigning solenoid


        //Assigning hall effects

        climbhallEffect = new DigitalInput(RobotMap.climb_hall_effect);

        direction = 0;
        distanceTicks = 0;
        distanceTicksReliable = false;
        initialEncoderValue = leftclimb_motor.getSelectedSensorPosition();

        //Up is negative, set the max and min height limits
        MAX_HEIGHT = -479;
        MIN_HEIGHT = 0;
    
    }

    @Override
    public void periodic() {
        
        //distanceTicks = climb_motor.getSelectedSensorPosition();
        //SmartDashboard.putNumber("Climb Encoder", distanceTicks);
        /*
        if (direction == 1 && hall_effect_top.get() == false){
            climb_motor.set(TalonFXControlMode.PercentOutput, 0);
            distanceTicksReliable = true;
            climb_motor.setSelectedSensorPosition(MAX_HEIGHT);

        } 
        if (direction == 2 && hall_effect_bottom.get() == false){
            climb_motor.set(TalonFXControlMode.PercentOutput, 0);
            distanceTicksReliable = true;
            climb_motor.setSelectedSensorPosition(MIN_HEIGHT);
        }

        if (distanceTicksReliable && ((distanceTicks < MAX_HEIGHT && direction == 1) || (distanceTicks > MIN_HEIGHT && direction == 2))){            
            climb_motor.set(TalonFXControlMode.PercentOutput, 0);
        }
        */

    }
    
    @Override
    public void simulationPeriodic() {}


    
    /**Moves the lift at -0.2 speed (up) */
    public void rotateClockwise() {
        //Move climber at speed -0.2 in the direction up
        leftclimb_motor.set(TalonFXControlMode.PercentOutput, -.4); //THIS MUST BE NEGATIVE
        rightclimb_motor.set(TalonFXControlMode.PercentOutput, .4);
        direction = 1;
    }


    /**Moves the lift at 0.2 speed (down) */
    public void rotateCounterClockwise() {
        //Move climber at speed 0.2 in the direction down
        leftclimb_motor.set(TalonFXControlMode.PercentOutput, .4);
        rightclimb_motor.set(TalonFXControlMode.PercentOutput, -.4);

        direction = 2;
    }

    public void stop(){
        leftclimb_motor.set(TalonFXControlMode.PercentOutput, 0);
        rightclimb_motor.set(TalonFXControlMode.PercentOutput, 0);
    }

    public void swing() {
        
        /*
        motor go up
        piston go on burrrrrr
        motor go up far
        piston go off burrrrr
        motor retract
        */

    }
    public void long_swing() {
        
        /*
        motor go up
        piston go on burrrrrr
        motor go up full
        piston go off burrrrr
        motor retract
        */
    }

    public boolean getHallSensor(){
       return climbhallEffect.get()==false;

    }
}