package frc.robot;

public class Constants {
    public static final double drivetrainTicksPerInch = 943.667;
    /*
     *SparkMax Values
     */
    public static final double kP = 0.00000033;
    public static final double kI = 0.00000014;
    public static final double kD = 0.001;
    public static final double kF = 0.0;
    /*
     *Drivetrain values
     */
    public static final int PID_PRIMARY = 0;
    public static final int PID_TURN = 1;
    public static final int REMOTE_0 = 0;
    public static final int REMOTE_1 = 1;

    public static final int kDistanceSlotIdx = 0;
    public static final int kDistancePIDLoopIDx = 0;
    public static final int kDistanceTimeoutMs = 30;
    public static final double kDistanceP = 0.2;
    public static final double kDistanceI = 0;
    public static final double kDistanceD = 0;
    public static final double kDistanceF = 0.2;
    public static final int kDistanceIZone = 0;
    public static final int kDistancePeakOutput = 1;

    public static final int kTurnSlotIdx = 1;
    public static final int kTurnPIDLoopIDx = 0;
    public static final int kTurnTimeoutMs = 30;
    public static final double kTurnP = 0.2;
    public static final double kTurnI = 0;
    public static final double kTurnD = 0;
    public static final double kTurnF = 0.2;
    public static final int kTurnIZone = 0;
    public static final int kTurnPeakOutput = 1;

    public static final double kNeutralDeadband = 1;
    /*
     *The Great Wheel Values
     */
    public static final int tSlotIdx = 0;
    public static final int tPIDLoopIDx = 0;
    public static final int tTimeoutMs = 30;
    public static final double tkP = 0.47;
    public static final double tkI = 0.0002;
    public static final double tkD = 0.00005;
    public static final double tkF = 0.0;
    public static final int tIzone = 0;
    public static final double tPeakOutput = 1;
     /*
     * HoodSet values
     */
    public static final int hoodSlotIdx = 0;
    public static final int hoodPIDLoopIDx = 0;
    public static final int hoodTimeoutMs = 30;
    public static final double hoodkP = 0.2;
    public static final double hoodkI = 0;
    public static final double hoodkD = 0;
    public static final double hoodkF = 0.2;
    public static final int hoodIzone = 0;
    public static final double hoodPeakOutput = 1;

    public static final int MAX_OBJECTS = 22;
}